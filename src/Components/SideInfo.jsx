import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setEmail, setPassword, setUsername } from "../Store/Slices/User";
import style from './SideInfo.css'


function SideInfo() {
    const user = useSelector((state) => state.user)
    const navigate = useNavigate();
    const [show, setShow] = useState(false);

    return (
        <div>
      <div className="SideInfo">
       <input type="checkbox" id="menu_checkbox"/>
        <label id="sidelabel" htmlFor="menu_checkbox" onClick={() => setShow(!show)}>
         <div></div>
         <div></div>
         <div></div>
        </label>
       
      
       
      </div>
      { show &&
        <div className="info">
            <p> {user.username}</p>
            <p> Activities: {user.completedToDos}</p>
            <p> Created By <br/>Joachim Nilsson<br/> @2022</p>
            <img src= 'https://www.codewars.com/users/Klutcharn/badges/micro'></img>
        </div>
         }
      </div>
    );
  }
  

export default SideInfo;

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setEmail, setPassword, setUsername, AddToDo} from "../Store/Slices/User";
import { setURL,setAmountOfPeople } from "../Store/Slices/Api";
import Landing from "./Landing";
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import style from './Main.css'
import { setPage } from "../Store/Slices/Site";
import Login from "./Login";


function Main() {

    
    const user = useSelector((state) => state.user)
    console.log(user)
    const URL = useSelector((state) => state.api)
    let [ToDo, setToDo] = useState([]);
    let [count, setCount] = useState(0)
    const [isLoading, setLoading] = useState(true);
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const [showSettings, setShowSettings] = useState(false);
    const [showadded, setShowadded] = useState(false);
    const [shownotadded, setShownotadded] = useState(false);
    let [number, setNumber] = useState(0)
    useEffect(() => {
        axios.get(URL.URL)
        .then(response => {
            console.log(response.data)
            setToDo(response.data)
        }
        ).then(() => setNumber(URL.amountOfPeople))
        .then(() => setLoading(false)).then(() => dispatch(setPage(0)))
    
     },[])

     function newAct(){
        axios.get(URL.URL)
        .then(response => {
            console.log(response.data)
            setToDo(response.data)
        }
        )
     }
     function showPopup(){
        if(!user.savedToDo.includes(ToDo.activity)){
        setTimeout(() => setShowadded(false),1000,setShowadded(true))
    } else {
        setTimeout(() => setShownotadded(false),1000,setShownotadded(true))
    }
    }
    
    const handleAmountOfPeople= (e) => {
        dispatch(setURL(e.target.value))
        dispatch(setAmountOfPeople(e.target.value))
        setNumber(e.target.value)

    }

    
  return (
      
    <div>
        
        
        {(user.username.length > 3) ?
       (
           <div>
                <Landing/>
        <div className="Main">
        <h1 className="welcome"> Welcome {user.username}!</h1>
        <button className="animation" id="settings" onClick={() => setShowSettings(!showSettings)}> <span>Change settings </span></button>
        { showSettings &&
        <div id="myModal" className="modal">
            <p>Number of participants needed: </p>
            <input className="input-style" type={'number'} value={number} max={5} min={1}onChange={handleAmountOfPeople}/>
        </div>
        }
        <div className="container">
        <h3>{ToDo.activity}</h3>
        <h4>People needed: {ToDo.participants}</h4>
        </div>
        <button className="animation" onClick={function() { dispatch(AddToDo(ToDo.activity)); showPopup()}}> <span>Save Activity</span></button>
        <button className="animation" onClick={newAct}><span> New Activity</span></button>
        { showadded &&
 <div className='popup'> 
 <p> You Saved A New Activity!</p>
</div>
      }
      { shownotadded &&
 <div className='popup'> 
 <p> Activity Already Saved!</p>
</div>
      }

    </div>
      </div>
    )
    : (<Login></Login>)
    }
    </div>
  );
}

export default Main;

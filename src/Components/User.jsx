import { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";

import { setPage } from "../Store/Slices/Site";
import {RemoveToDo,FinishToDo  } from "../Store/Slices/User";
import Landing from "./Landing";
import Login from "./Login";
import style from './User.css'


function User() {
    const dispatch = useDispatch()
    const user = useSelector((state) => state.user)
    
useEffect(() =>{
  dispatch(setPage(1))
},[])
   
  return (
   <div>
      {(user.username.length > 3) ?
       (
           <div>
     <Landing/>
     <div className="User">
   <div className="activity">
    <h3>Finished activities: {user.completedToDos}</h3>
  
    
    {!user.overTen &&
     <h5>Get 10 activities done!</h5>
    }
        {user.overTen &&
     <h5>Get 25 activities done!</h5>
    }


    {!user.overTen &&
    <progress value={user.completedToDos} max="10" ></progress>
    }
        {user.overTen &&
    <progress value={user.completedToDos} max="25" ></progress>
    }
     <h2> Saved Hobbies</h2>
     {user.savedToDo.map(function(d, idx){
         return (<div className="cont"  key={idx} >
            <div className='activityContainer'>
            <p className="actText">{d}</p>
            <button onClick={function(){ dispatch(FinishToDo());dispatch(RemoveToDo(idx)) }}><span> Finish Activity </span></button>
           <button onClick={() => dispatch(RemoveToDo(idx))}><span> Remove Activity</span></button>
           </div>
             </div>)
       })}
</div>
    </div>
    </div>
       ) : (<Login/>)
      }
    </div>
  );
}

export default User;

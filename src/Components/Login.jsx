import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setEmail, setPassword, setUsername,clearToDo } from "../Store/Slices/User";
import style from './Login.css'


function Login() {


    const [username, setUser] = useState('');
    const [password, setPass] = useState('');
    const [email, setEm] = useState('');

    const handleUsername= (e) => setUser(e.target.value);
    const handlePassword= (e) => setPass(e.target.value);
    const handleEmail= (e) => setEm(e.target.value);
    
    const navigate = useNavigate();
    const dispatch = useDispatch()
    const onSubmit = (event) => {
        event.preventDefault(); //stop the page reload (since we have a single page app)
        console.log("logging")
        dispatch(setUsername(username));
        dispatch(setPassword(password));
        dispatch(setEmail(email));
        dispatch(clearToDo()); 
        navigate("/main")
        }
      
    


  return (
    <div className="login">
        <h1>Welcome</h1>
        <h3>Enter Username</h3>
<form onSubmit={onSubmit}>
<div>
  <div>
    <input className="UserName" type="text" name="name"  minLength={'3'} required placeholder="Username" onChange={handleUsername}/>
    </div>

    {/* <input type="password" name="name"  minLength={'3'} placeholder="Password" onChange={handlePassword}/> */}

  
    {/* <input type="email" name="name" placeholder="Email" onChange={handleEmail}/> */}
  <div>
  <button type="submit"><span> Log In </span></button>
  </div>
       </div>
  </form>
    </div>
  );
}

export default Login;

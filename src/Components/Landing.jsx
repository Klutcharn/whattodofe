import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setEmail, setPassword, setToDo, setUsername } from "../Store/Slices/User";
import SideInfo from "./SideInfo";
import style from './Landing.css'



function Landing() {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const page = useSelector((state) => state.site.page)

    return (
      <div className="Landing">
        <SideInfo/>
        <div className="Land">
          {page === 0 && 
           <button className="landingshine" onClick={ () => navigate('/main')}> <span>Main </span></button>
           }
           {page === 1 && 
           <button className="landing" onClick={ () => navigate('/main')}> <span>Main </span></button>
           }
           {page === 0 && 
            <button className="landing" onClick={ () => navigate('/user')}> <span>User </span></button>
           }
           {page === 1 && 
            <button className="landingshine" onClick={ () => navigate('/user')}> <span>User </span></button>
           }

       
       
        <button className="landing" onClick={function() { navigate('/');dispatch(setToDo(0))}}> <span>Logout </span></button>
        </div>
      </div>
    );
  }
  

export default Landing;

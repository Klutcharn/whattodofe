import { configureStore } from '@reduxjs/toolkit'
import userReducer from './Slices/User'
import apiReducer from './Slices/Api'
import siteReducer from './Slices/Site'

export const store = configureStore({
  reducer: {
      user: userReducer,
      api: apiReducer,
      site: siteReducer
  },
})
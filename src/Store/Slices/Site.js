import { createSlice } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'

const initialState = {
    page: 0,
}


export const siteSlice = createSlice({
    
  name: 'site',
  initialState,
  reducers: {
    setPage: (state, action) => {
      state.page = action.payload

    }
  },
})

// Action creators are generated for each case reducer function
export const {setPage } = siteSlice.actions

export default siteSlice.reducer
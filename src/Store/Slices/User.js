import { createSlice } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'

const initialState = {
  username: "",
  password: "",
  email: "",
  savedToDo: [],
  completedToDos: 0,
  overTen: false,
  added: false,
  notadded: false
}


export const userSlice = createSlice({
    
  name: 'user',
  initialState,
  reducers: {
    setUsername: (state, action) => {
        console.log(action.payload)
      state.username = action.payload
    },
    setPassword: (state,action) => {
        console.log(action.payload)
      state.password = action.payload
    },
    setEmail: (state, action) => {
        console.log(action.payload)
      state.email = action.payload
    },
    setAdded:(state, action) =>{
        state.added = action.payload
    },
    clearToDo:(state) =>{
        state.savedToDo = []
    },
    AddToDo: (state, action) =>{
        if(!state.savedToDo.includes(action.payload)){
            state.savedToDo.push(action.payload)
           
        }
        console.log(action.payload)
    },
    RemoveToDo: (state, action) =>{
        if(action.payload == 0){
            state.savedToDo.shift()
        }
        state.savedToDo.splice(action.payload,action.payload)
        console.log(state.savedToDo)
    },
    FinishToDo: (state) =>{
        state.completedToDos++
        if(state.completedToDos >= 10){
            state.overTen = true;
        }
    },
    setToDo:(state, action) =>{
      state.completedToDos = action.payload
      if(action.payload >= 10){
        state.overTen = true
      } else{
        state.overTen = false
      }
    }
  },
})

// Action creators are generated for each case reducer function
export const {setUsername, setPassword,setEmail,clearToDo,AddToDo,RemoveToDo,FinishToDo, setToDo } = userSlice.actions

export default userSlice.reducer
import { createSlice } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'

const initialState = {
    URL: "http://www.boredapi.com/api/activity?participants=1",
    amountOfPeople: 1
}


export const apiSlice = createSlice({
    
  name: 'api',
  initialState,
  reducers: {
    setURL: (state, action) => {
      state.URL = 'http://www.boredapi.com/api/activity?participants='+action.payload
      state.amountofPeople = action.payload
    },
    setAmountOfPeople: (state, action) =>{
        state.amountOfPeople = action.payload
    }
   
  },
})

// Action creators are generated for each case reducer function
export const {setURL,setAmountOfPeople } = apiSlice.actions

export default apiSlice.reducer
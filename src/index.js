import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App.js';
import reportWebVitals from './reportWebVitals';
import { store } from './Store/Store.js'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Routes, Switch } from 'react-router-dom'
import Login from './Components/Login'
import User from './Components/User';
import Main from './Components/Main';

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
 
  <Router>
    <Routes>
    <Route  path="/" element={<Login/>}></Route>
    <Route path="/user" element={<User/>}></Route>
    <Route path="/main" element={<Main/>} />
    </Routes>
  </Router>
  </Provider>
  
)



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
